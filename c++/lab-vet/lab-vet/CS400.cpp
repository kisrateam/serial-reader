#include "CS400.h"
#include <thread>


CS400::~CS400()
{

}

void CS400::ack()
{
	char ack = ACK;
	this->SP->WriteData(&ack, 1);
	KUtils::write_text_to_log_file("ACK", this->name, true, true);
}

string CS400::CheckSum(string s)
{

	try {
		byte checksum = 0;
		char hex_str[3];
		for (int i = 0; i <= (int)s.length(); i++)
		{
			checksum += s[i];
		}
		sprintf(hex_str, "%02X", checksum);

		return hex_str;
	}
	catch (const std::exception& e) {
		KUtils::write_text_to_log_file("CheckSum\n" + string(e.what()), this->name, true, true);
	}
}

string CS400::Packaging(string record)
{
	string strSum = "";
	record = record + (char)CR + (char)ETX;
	strSum = CheckSum(record);
	record = (char)STX + record + strSum + (char)CR + (char)LF;
	return record;
}

string CS400::EndPackaging(string record)
{
	string strSum = "";
	record = record + (char)CR + (char)ETX;
	strSum = CheckSum(record);
	record = record + strSum + (char)CR + (char)LF;
	return record;
}

string CS400::PackagingOnlyCR(string record)
{
	record = record + (char)CR;
	return record;
}

string CS400::CreatePacketing(KJson::CS400Request data)
{
	string strSum = "";
	string record = "";
	string strOrder = "";
	string age = "";
	string record_sum = "";
	time_t now = time(0);
	tm* ltm = localtime(&now);
	char date_time[15];
	sprintf(date_time, "%d%02d%02d%02d%02d%02d", ltm->tm_year + 1900, ltm->tm_mon + 1, ltm->tm_mday, ltm->tm_hour, ltm->tm_min, ltm->tm_sec);
	string time_now = date_time;
	string ansiName = KUtils::utf8_to_string(data.name.c_str(), locale(".874"));
	//create test order
	for (int j = 0; j < (int)data.orders.size(); j++)
	{
		if (j > 0) {
			strOrder += "\\";
		}
		strOrder += data.orders[j]+"^^^";
	}
	//age
	age += "^"+data.age+"^"+data.age_unit;
	vector<string> all_record = {
		"1H|\\^&|||Mindray^^||||||Automated Count^00001|SA|1394-97|",
		"P|1||"+ data.number +"||^"+ data.name +"^^||"+ age +"|"+ data.sex +"||||||||||||||||||||||||||",
		"O|1||"+ data.barcode +"|"+ strOrder +"|R||||||||||||||||||||Q|||||",
		"L|1|N"
	};
	for (auto& s : all_record) {
		record += PackagingOnlyCR(s);
		record_sum += s;
	}
	strSum = CheckSum(record + (char)ETX);
	record = (char)STX + record + (char)ETX + strSum + (char)CR + (char)LF;
	return record;
}

string CS400::CreateQPacketing()
{
	string strSum = "";
	string record = "";
	string record_sum = "";
	time_t now = time(0);
	tm* ltm = localtime(&now);
	char date_time[15];
	sprintf(date_time, "%d%02d%02d%02d%02d%02d", ltm->tm_year + 1900, ltm->tm_mon + 1, ltm->tm_mday, ltm->tm_hour, ltm->tm_min, ltm->tm_sec);
	int SN = 0;
	//SN = stoi(Sequenc_Number)+1;
	string time_now = date_time;
	vector<string> all_record = {
		"1H|\^&|3053||Mindray^^||||||Worksheet Request^00010|RQ|1394-97|20221130205404",
		"Q|3053|^1234567891||||||||||O",
		"L|1|N"
	};
	for (auto& s : all_record) {
		record += PackagingOnlyCR(s);
		record_sum += s;
	}
	strSum = CheckSum(record + (char)ETX);
	record = (char)STX + record + (char)ETX + strSum + (char)CR + (char)LF;
	return record;
}

string CS400::HeaderPacket(int recordNumber)
{
	string record = "";
	record = to_string(recordNumber) + "H" + pipe + delimiterDef;

	//return Packaging(record);
	return PackagingOnlyCR(record);
}

string CS400::PatientInformationPacket(int recordNumber, int sequenceNumber, string sample_id, string name, string sex, string age, string age_unit)
{
	string record = "";
	//record = to_string(recordNumber) + "P" + pipe + to_string(sequenceNumber) + string(2, pipe) + sample_id + string(2, pipe) + name + string(3, pipe) + sex + string(6, pipe) + age + caret + age_unit;
	record = "P" + pipe + to_string(sequenceNumber) + string(2, pipe) + sample_id + string(2, pipe) + name + string(3, pipe) + sex + string(6, pipe) + age + caret + age_unit;
	//return Packaging(record);
	return PackagingOnlyCR(record);
}

string CS400::TestOrderPacket(int recordNumber, int sequenceNumber, string sample_id, string sample_no, string disk_id, string position_no, string diluent, string universal_test, string piority, string specimen_descriptor, string report_type)
{
	time_t now = time(0);
	tm *ltm = localtime(&now);
	char date_time[15];
	sprintf(date_time, "%d%02d%02d%02d%02d%02d", ltm->tm_year + 1900, ltm->tm_mon + 1, ltm->tm_mday, ltm->tm_hour, ltm->tm_min, ltm->tm_sec);
	string record = "";
	//record = to_string(recordNumber) + "O" + pipe + to_string(sequenceNumber) + pipe + sample_id + caret + sample_no + caret + disk_id + caret + position_no + caret + diluent + string(2, pipe) + string(3, caret) + universal_test + pipe + piority + pipe + date_time + string(9, pipe) + specimen_descriptor + string(10, pipe) + report_type;
	record = "O" + pipe + to_string(sequenceNumber) + pipe + sample_id + caret + sample_no + caret + disk_id + caret + position_no + caret + diluent + string(2, pipe) + string(3, caret) + universal_test + pipe + piority + pipe + date_time + string(9, pipe) + specimen_descriptor + string(10, pipe) + report_type;

	//return Packaging(record);
	return PackagingOnlyCR(record);
}

string CS400::TerminatorPacket(int recordNumber, int sequenceNumber, string termination_code)
{
	string record = "";
	//record = to_string(recordNumber) + "L" + pipe + to_string(sequenceNumber) + pipe + termination_code;
	record = "L" + pipe + to_string(sequenceNumber) + pipe + termination_code;
	return Packaging(record);
}


void CS400::writeTest(KSerial* sp, string str)
{	
	const char* packet = str.c_str();
	const char eot = EOT;
	const char enq = ENQ;
	char c;
	int couter = 0;
	KUtils::write_text_to_log_file("Before send DATA", this->name + "_test_send", true, true);
	retry_data:
	couter++;
	SP->WriteData(packet, (int)str.length());
	KUtils::write_text_to_log_file("Send DATA", this->name+"_test_send", true, true);
	while (!sp->ReadData(&c));
	if (c == NAK)
	{
		KUtils::write_text_to_log_file("Receive NAK", this->name + "_test_send", true, true);
		retry_nak:
		sp->WriteData(&enq, 1);
		KUtils::write_text_to_log_file("Send ENQ", this->name + "_test_send", true, true);
		while (!sp->ReadData(&c));
		if (c == NAK)
		{
			KUtils::write_text_to_log_file("Recive nak 2", this->name + "_test_send", true, true);
			goto retry_nak;
		}
		else if (c == ACK)
		{
			KUtils::write_text_to_log_file("Recive ack 2", this->name + "_test_send", true, true);
			if (couter < 3) {
				goto retry_data;
			}
			KUtils::write_text_to_log_file("Send eot 2", this->name + "_test_send", true, true);
		}
		else {
			KUtils::write_text_to_log_file("Recive not ack 2", this->name + "_test_send", true, true);
		}
	}
	else if (c == ACK)
	{
		KUtils::write_text_to_log_file("Recive ack 3", this->name + "_test_send", true, true);
		SP->WriteData(&eot, 1);
		KUtils::write_text_to_log_file("Send EOT 3", this->name + "_test_send", true, true);
	}
	else {
		KUtils::write_text_to_log_file("Recive not ack 3", this->name + "_test_send", true, true);
	}

}

void CS400::write(KSerial* sp, string str)
{
	const char *packet = str.c_str();
	char c;
	while (!sp->ReadData(&c));
	if (c == ACK)
	{
		//KUtils::write_text_to_log_file("Receive ACK", this->name, true, true);
		KUtils::write_text_to_log_file("Receive ACK write", this->name + "_test_send", true, true);
		sp->WriteData(packet, (int)str.length());
	}
}

void CS400::clearBuffer(char  packetBuff[1024], char  packetData[1024])
{
	memset(packetBuff, 0, sizeof packetBuff);
	memset(packetData, 0, sizeof packetData);
}

int CS400::readSerial()
{

	char c = 0;
	char packetBuff[1024] = "";
	char all_text[1024] = "";
	char packetData[1024] = "";
	int packetLength = 0;
	int packetCheck = 0;
	byte sum = 0;
	int dataLength = 1023;
	int readResult = 0;
	int state = 0;
	char checksumStr[3] = "";
	char recordType = 0;
	char mode = 0;
	char modeQ = 0;
	char modeL = 0;
	char modeEOT = 0;
	char requestOrderSum = 1;
	string diskNo = "";
	string posNo = "";
	string patient_name = "";
	string test1 = "";
	string test2 = "";
	string test3 = "";
	string test4 = "";
	string Sequenc_Number = "";
	bool isBusy = false;
	bool stop_send = false;
	clock_t time_start = clock();


	vector<string> specimen;
	vector<string> instrumentSpecimentID;
	vector<string> universalTestID;
	vector<string> sample;
	vector<string> refRange;
	vector<string> value;
	vector<string> MeasurementValue;
	vector<string> MeasurementRange;
	string MeasurementRangeUpperLimit;
	string MeasurementRangeLowerLimit;
	vector<string> normalRange;
	vector<string> panicRange;
	vector<vector<string>> requestOrder;
	vector<string> field;
	vector<string> all_list;

	int counter = 0;
	int onStart = 0;

	while (this->SP->IsConnected())
	{
		try
		{
			readResult = this->SP->ReadData(&c);
		}
		catch (const std::exception& e)
		{
			KUtils::write_text_to_log_file("read serial\n" + string(e.what()), this->name, true, true);
		}
		
		bool test = true;
		if (readResult > 0)
		{
			string s(1, c);
			KUtils::write_text_to_log_file(s, this->name + "_serial", false, false);
			if (c == ENQ)
			{
				isBusy = true;
				KUtils::write_text_to_log_file("Receive ACK", this->name, true, true);
				sum = 0;
				packetLength = 0;
				state = 0;
				ack();
			}
			else if (c == STX)
			{
				sum = 0;
				packetLength = 0;
				state = 1;
			}
			else if (c == EOT)
			{
				modeEOT = c;
				if (modeQ == 'Q' && modeL == 'L') {
					modeQ = 0;
					modeL = 0;
					const char enq = ENQ;
					const char eot = EOT;
					KUtils::write_text_to_log_file("Check barcode", this->name + "_download", true, true);
					KJson::CS400Request data;
					for (int i = 0; i < (int)requestOrder.size(); i++)
					{
						const string barcode = requestOrder[i][1];
						if (barcode.length() == 0)
						{
							KUtils::write_text_to_log_file("Barcode not found", this->name + "_download", true, true);
							jsonObj->jsonDecodeCS400(jsonObj->defaultData);
							data = jsonObj->cs400Request;
						}
						else
						{
							KUtils::write_text_to_log_file("Barcode found", this->name + "_download", true, true);
							string jsonData;
							CURLcode responseCode = this->api->requestCS400(barcode, &jsonData);
							if (!responseCode)
							{
								if (jsonData == "404")
								{
									KUtils::write_text_to_log_file("API not found", this->name + "_download", true, true);
									cout << "404" << endl;
									jsonObj->jsonDecodeCS400(jsonObj->defaultData);
									data = jsonObj->cs400Request;
								}
								else
								{
									jsonObj->jsonDecodeCS400(jsonData);
									data = jsonObj->cs400Request;
								}
							}
							else
							{
								KUtils::write_text_to_log_file("API found", this->name + "_download", true, true);
								jsonObj->jsonDecodeCS400(jsonObj->defaultData);
								data = jsonObj->cs400Request;
							}
						}
					}

					SP->WriteData(&enq, 1);
					KUtils::write_text_to_log_file("Send ENQ", this->name + "_download", true, true);
					char c;
					KUtils::write_text_to_log_file("Start wait ACK", this->name + "_download", true, true);
					while (!SP->ReadData(&c));
					if (c == NAK)
					{
						KUtils::write_text_to_log_file("Receive NAK", this->name + "_download", true, true);
						//goto start_q;
					}
					else if (c == ACK) {
						KUtils::write_text_to_log_file("Receive ACK", this->name + "_download", true, true);
						string test_str = "";
						KUtils::write_text_to_log_file("Send Data", this->name + "_download", true, true);
						test_str = CreatePacketing(data);
						const char* packet = test_str.c_str();
						SP->WriteData(packet, (int)test_str.length());
						while (!SP->ReadData(&c));
						if (c == ACK)
						{
							KUtils::write_text_to_log_file("Send EOT", this->name + "_download", true, true);
							SP->WriteData(&eot, 1);
						}
					}
					else {
						KUtils::write_text_to_log_file("Receive Something", this->name + "_download", true, true);
					}

					requestOrder.clear();
					mode = 0;
					sum = 0;
					packetLength = 0;
					state = 0;
					clearBuffer(packetBuff, packetData);
					isBusy = false;
					time_start = clock();
				}
			}

			if (state)
			{
				packetBuff[packetLength] = c;
				packetLength++;
				sum += c;

				if (packetBuff[packetLength - 3] == ETX)
				{
					sum = sum - STX - packetBuff[packetLength - 2] - packetBuff[packetLength - 1];
					memcpy(checksumStr, &packetBuff[packetLength - 2], 2);
					int a = std::stoi(checksumStr, 0, 16);
					if (sum != a)
					{
						state = 0;
						sum = 0;
						packetLength = 0;
						continue;
					}
					else
					{
						memcpy(packetData, &packetBuff[1], packetLength - 5);
						stop_send = false;
						//all_list.clear();
						//all_list.resize(0);
						//all_list.erase(std::remove(all_list.begin(), all_list.end(), ""), all_list.end());
						//CR
						//vector<string> all_list;
						all_list = KUtils::split(packetData, 0x0D);
						//LF
						//all_list = KUtils::split(packetData, 0x0A);
						
						int check_first = 0;

						clearBuffer(packetBuff, packetData);

						for (auto& s : all_list) {
							if (check_first == 0) {
								recordType = s[1];
								std::cout << s[1] << std::endl;
							}
							else {
								recordType = s[0];
								std::cout << s[0] << std::endl;
							}
							check_first++;

							field = KUtils::split(s, '|');

							switch (recordType)
								{
								case 'H':
									KUtils::write_text_to_log_file("Received Header Record", this->name, true, true);
									//ack();
									break;
								case 'P':
									KUtils::write_text_to_log_file("Received Patient Record ", this->name, true, true);
									patient_name = KUtils::split(field[5], '^')[0];
									KUtils::write_text_to_log_file("P "+ patient_name, "object", true, true);
									//ack();
									break;
								case 'O':
									KUtils::write_text_to_log_file("Received Order Record", this->name, true, true);
									instrumentSpecimentID = KUtils::split(field[4], '^');
									//try
									//{
										KUtils::write_text_to_log_file("O " + field[3] + " " + field[6], "object", true, true);
										KUtils::write_text_to_log_file("O " + instrumentSpecimentID[0] +" "+ instrumentSpecimentID[1], "object", true, true);

										if (patient_name == "QC")
										{
											jsonObj->addOrderBS620(field[3], field[6], this->name, patient_name, instrumentSpecimentID[0], instrumentSpecimentID[1]);
										}
										else
										{
											jsonObj->addOrderBS620(field[3], field[6], this->name, "", "", "");
										}
									//}
									//catch (const std::exception& e)
									//{
									//	KUtils::write_text_to_log_file("O catch" + field[3] + " " + field[6], "object", true, true);
									//	KUtils::write_text_to_log_file("O catch" + instrumentSpecimentID[0] + " " + instrumentSpecimentID[1], "object", true, true);
									//	if (patient_name == "QC")
									//	{
									//		jsonObj->addOrder(field[3], field[6], this->name, patient_name, "", "");
									//	}
									//	else
									//	{
									//		jsonObj->addOrder(field[3], field[6], this->name, "", "", "");
									//	}
									//}

									//ack();
									break;
								case 'R':
									mode = recordType;
									KUtils::write_text_to_log_file("Received Result Record", this->name, true, true);

									universalTestID = KUtils::split(field[2], '^');
									value = KUtils::split(field[3], '^');

									KUtils::write_text_to_log_file("R " + universalTestID[0] +" "+ value[0] + " " + field[4] + " " + field[6], "object", true, true);

									//old
									if (patient_name == "QC")
									{
										//jsonObj->addResult(universalTestID[3], field[3], field[4], field[6], panicRange[0], normalRange[0], normalRange[1], field[7]);
										//new
										jsonObj->addResultBS620(universalTestID[0], value[0], field[4], field[6], "", "", "", "");
									}
									else
									{
										//jsonObj->addResult(universalTestID[3], field[3], field[4], field[6], panicRange[0], normalRange[0], normalRange[1], panicRange[1]);
										//new
										jsonObj->addResultBS620(universalTestID[0], value[0], field[4], field[6], "", "", "", "");
									}

									//ack();
									break;
								case 'L':
									//KUtils::write_text_to_log_file("Received Terminate Record", this->name, true, true);
									//KUtils::write_text_to_log_file("Switch to L", "object", true, true);
									if (mode == 'R' && !stop_send)
									{
										//KUtils::write_text_to_log_file("Send data to API", "object", true, true);
										jsonObj->finish();
										this->api->jsonData = jsonObj->jsonData;
										//this->api->resultRawData();
										KUtils::write_text_to_log_file("Response = " + to_string(this->api->resultRawData()), this->name, true, true);
										//std::this_thread::sleep_for(300ms);
										//skip_send:
										jsonObj->clear();
										//KUtils::write_text_to_log_file("L clear " + jsonObj->jsonData, "object", true, true);
										stop_send = true;
									}
									//else if (mode == 'R' && stop_send) {
									//	jsonObj->clear();
									//}

									//ack();
									modeL = 'L';
									break;
								case 'Q':
									mode = recordType;
									KUtils::write_text_to_log_file("Received Request Information Record", this->name, true, true);

									sample = KUtils::split(field[2], '^');
									Sequenc_Number = field[1];
									requestOrder.push_back(sample);
									//ack();
									modeQ = 'Q';
									break;
								default:
									break;
							}
						}
						KUtils::write_text_to_log_file("Send ack after Receive", this->name, true, true);
						ack();
					
						state = 0;
						sum = 0;
						packetLength = 0;
					}
				}
			}
		}
		if (!isBusy)
		{
			clock_t elapse = (clock() - time_start) / 1000;
			if (elapse == 10)
			{
				ack();
				time_start = clock();
			}
		}
	}

	return 0;
}
