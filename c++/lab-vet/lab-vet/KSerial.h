#pragma once
#include <windows.h>
#include <iostream>
#include <string>


using namespace std;

class KSerial
{
private:
	//Serial comm handler
	HANDLE hSerial;
	//Connection status
	bool connected;
	//Get various information about the connection
	COMSTAT status;
	//Keep track of last error
	DWORD errors;
public:
	KSerial(string name, LPCWSTR portName, DCB dcbSerial);
	~KSerial();

	int ReadData(char *buffer);
	bool WriteData(const char * buffer, unsigned int nbChar);
	bool IsConnected();

	


	string name;
	LPCWSTR portName;
	DCB dcbSerial;

};

