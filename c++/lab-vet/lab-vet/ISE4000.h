#include "KDevice.h"

class ISE4000 : public KDevice
{
public:
	ISE4000(string comport, int buadrate) : KDevice("ISE4000", comport, buadrate)	{}
	~ISE4000();

	int readSerial();
};

