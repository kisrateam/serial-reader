#include "BC5000.h"
#include <iostream>
#include <cstring>
#include <winsock2.h>
#include <sys/types.h>
#include <fstream>

#pragma comment(lib, "ws2_32.lib")
#pragma warning(disable:4996) 
BC5000::~BC5000()
{
}

int BC5000::tcpReceive()
{
	cout << "Start Client Version";
	// Initialize Winsock
	WSADATA wsa_data;
	int result = WSAStartup(MAKEWORD(2, 2), &wsa_data);
	if (result != 0) {
		std::cerr << "Failed to initialize Winsock.\n";
		Sleep(10000);
		return 1;
	}
	// Create a socket
	SOCKET sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (sock == INVALID_SOCKET) {
		std::cerr << "Failed to create socket.\n";
		WSACleanup();
		Sleep(10000);
		return 1;
	}
	// Prepare the sockaddr_in structure
	struct sockaddr_in server;
	server.sin_family = AF_INET;
	// Port number to use
	server.sin_port = htons(5100); 
	// IP address of the server
	server.sin_addr.s_addr = inet_addr("11.0.0.2");
	// Connect to the server
	if (connect(sock, (struct sockaddr*)&server, sizeof(server)) == SOCKET_ERROR) {
		std::cerr << "Failed to connect to server.\n";
		closesocket(sock);
		WSACleanup();
		Sleep(10000);
		return 1;
	}
	char buffer[4096];
	while (true) {
		memset(buffer, 0, sizeof(buffer)); // Clear the buffer
		int bytes_received = recv(sock, buffer, sizeof(buffer), 0);
		if (bytes_received < 0) {
			std::cerr << "Failed to receive data from server.\n";
			closesocket(sock);
			WSACleanup();
			return 1;
		}
		//std::ofstream outfile("bc5000.txt");
		//outfile.write(buffer, sizeof(buffer));
		//outfile.close();
	
		std::cout << "Received " << bytes_received << " bytes: " << buffer << "\n";
		decodeProcess(buffer);
	}
	// Close the socket
	closesocket(sock);
	// Cleanup Winsock
	WSACleanup();
	cout << "End";
	Sleep(10000);
	return 0;

	//cout << "Start Server Version";
	//while (net->active) {
	//	//start accepting clients ------------------------------
	//	net->len = sizeof(net->server);
	//	net->client_fd = accept(net->server_socket, (struct sockaddr*)&net->server, &net->len);
	//	//our client is a real thing?
	//	if (net->client_fd != INVALID_SOCKET) {
	//		cout << "New client";
	//		//save client socket into our struct table
	//		string ip = getIP();
	//		net->clients[net->clients_connected].ss = net->client_fd;
	//		net->clients[net->clients_connected].connected = TRUE;
	//		//and of course we need a calculator too
	//		net->clients_connected++;
	//		cout << "New client: " << net->client_fd << " IP: " << ip << endl;
	//	}
	//	//plus we don't need to loop that fast anyways
	//	Sleep(1);
	//	//we have clients or no?
	//	if (net->clients_connected > 0) {
	//		//lets go through all our clients
	//		for (int cc = 0; cc < net->clients_connected; cc++) {
	//			memset(&net->recvbuf, 0, sizeof(net->recvbuf));
	//			if (net->clients[cc].connected) {
	//				//receive data
	//				net->receiveres = recv(net->clients[cc].ss, net->recvbuf,
	//					DEFAULT_BUFLEN, 0);
	//				//and echo it back if we get any
	//				if (net->receiveres > 0) {
	//					Sleep(10);
	//					KUtils::write_text_to_log_file(net->recvbuf, "bc5000", true, true);
	//					decodeProcess();
	//				}
	//				else if (net->receiveres == 0 || strcmp(net->recvbuf,
	//					"disconnect") == 0) {
	//					cout << "Client disconnected." << endl;
	//					net->clients[cc].connected = FALSE;
	//					net->clients_connected--;
	//					//delete [cc] clients;
	//				}
	//			}
	//		}
	//	}
	//}
	////when we shut down our server
	//closesocket(net->server_socket);
	//// Clean up winsock
	//WSACleanup();
	//return 0;
}

//client
void BC5000::decodeProcess(string buffer) {
//server
//void BC5000::decodeProcess() {
	vector<string> record;
	vector<string> column;
	_MSH2 msh;
	_PID2 pid;
	_PV12 pv1;
	_OBR2 obr;
	_OBX2 obx;
	//client
	record = KUtils::split(buffer, 0x0D);
	//server
	//record = KUtils::split(net->recvbuf, 0x0D);
	//if (record[0] != "\x2\x2\x2") {
		for (int i = 0; i < record.size(); i++)
		{
			record[i].erase(std::remove_if(record[i].begin(), record[i].end(), [](char c) {return c == '\x2'; }), record[i].end());
			column = KUtils::split(record[i], this->pipe);
			//if (column[0] == "\x0MSH")
			if (column[0] == "\vMSH")
			{
				msh = decodeMSH2(record[i]);
			}
			else if (column[0] == "PID")
			{
				pid = decodePID2(record[i]);
			}
			else if (column[0] == "PV1")
			{
				//pv1 = decodePV1(record[i]);
			}
			else if (column[0] == "OBR")
			{
				string ip = getIP();
				obr = decodeOBR2(record[i]);
				jsonObj->addOrder(obr.FillerOrderNumber, msh.DateTimeOfMessage, this->name, obr.PrincipalResultInterpreter, ip, "");
			}
			else if (column[0] == "OBX")
			{
				obx = decodeOBX2(record[i]);

				if (obx.ValueType == "NM")
				{
					vector<string> observationIdentifier = KUtils::split(obx.ObservationIdentifier, '^');
					vector<string> referencesRange = KUtils::split(obx.ReferencesRange, '-');
					vector<string> abnormalFlags = KUtils::split(obx.AbnormalFlags, '~');

					if (observationIdentifier[1] != "Age")
					{
						jsonObj->addResult(observationIdentifier[1], obx.ObservationValue, obx.Units, abnormalFlags[1], "", referencesRange[0], referencesRange[1], "");
					}
				}

			}
			//else if (column[0] == "\x1c")
			else if (column[0] == "\x1c")
			{
				jsonObj->finish();
				api->jsonData = jsonObj->jsonData;
				KUtils::write_text_to_log_file(api->jsonData, "bc5000_obj", true, true);
				cout << "Response = " << api->resultRawData2() << endl;
				jsonObj->clear();
				memset(net->recvbuf, 0, sizeof net->recvbuf);
			}
		}
	//}
}

char* BC5000::getIP()
{
	char* s = NULL;
	s = (char*)malloc(INET_ADDRSTRLEN);
	inet_ntop(AF_INET, &(net->server.sin_addr), s, INET_ADDRSTRLEN);
	return s;
}

_MSH2 BC5000::decodeMSH2(string field)
{
	vector<string> mshData = KUtils::split(field, this->pipe);
	_MSH2* msh = new _MSH2();
	if (mshData.size() >= 17) {
		msh->FieldSeparator = mshData[0];
		msh->EncodingCharacters = mshData[1];
		msh->Sendingapplication = mshData[2];
		msh->SendingFacility = mshData[3];
		msh->DateTimeOfMessage = mshData[6];
		msh->MessageType = mshData[8];
		msh->MessageControlID = mshData[9];
		msh->ProcessingID = mshData[10];
		msh->VersionID = mshData[11];
		msh->CharacterSet = mshData[17];
	}
	else
	{
		msh = NULL;
	}
	return *msh;
}

_PID2 BC5000::decodePID2(string field)
{
	vector<string> pidData = KUtils::split(field, this->pipe);
	_PID2* pid = new _PID2();
	if (pidData.size() >= 8) {
		pid->SetIDPID = pidData[1];
		pid->PatientIdentifierList = pidData[3];
		pid->PatientName = pidData[5];
		pid->DateTimeofBirth = pidData[7];
		pid->Sex = pidData[8];
	}
	else
	{
		pid->SetIDPID = pidData[1];
		//pid = NULL;
	}
	return *pid;
}

_PV12 BC5000::decodePV12(string field)
{
	vector<string> mshData = KUtils::split(field, this->pipe);
	_PV12* pv1 = new _PV12();
	return *pv1;
}

_OBR2 BC5000::decodeOBR2(string field)
{
	vector<string> obrData = KUtils::split(field, this->pipe);
	_OBR2* obr = new _OBR2();
	if (obrData.size() >= 32) {
		obr->SetIDOBR = obrData[1];
		obr->PlacerOrderNumber = obrData[2];
		obr->FillerOrderNumber = obrData[3];
		obr->UniversalServiceID = obrData[4];
		obr->RequestedDatetime = obrData[6];
		obr->ObservationDateTime = obrData[7];
		obr->CollectorIdentifier = obrData[10];
		obr->RelevantClinicalInfo = obrData[13];
		obr->SpecimenReceivedDateTime = obrData[14];
		obr->SpecimenSource = obrData[15];
		obr->ResultsRptStatusChngDateTime = obrData[22];
		obr->DiagnosticServSectID = obrData[24];
		obr->ResultCopiesTo = obrData[28];
		obr->PrincipalResultInterpreter = obrData[32];
	}
	else
	{
		obr = NULL;
	}
	return *obr;
}

_OBX2 BC5000::decodeOBX2(string field)
{
	vector<string> obxData = KUtils::split(field, this->pipe);
	_OBX2* obx = new _OBX2();
	if (obxData.size() >= 11) {
		obx->SetIDOBX = obxData[1];
		obx->ValueType = obxData[2];
		obx->ObservationIdentifier = obxData[3];
		obx->ObservationValue = obxData[5];
		obx->Units = obxData[6];
		obx->ReferencesRange = obxData[7];
		obx->AbnormalFlags = obxData[8];
		obx->ObservResultStatus = obxData[11];
	}
	else
	{
		obx = NULL;
	}
	return *obx;
}



