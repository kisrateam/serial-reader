
#include "KSerial.h"
#include "KUtils.h"

constexpr auto STX = 0x02;
constexpr auto ETX = 0x03;
constexpr auto CR = 0x0D;
constexpr auto LF = 0x0A;



KSerial::KSerial(string name, LPCWSTR portName, DCB dcbSerial)
{
	//We're not yet connected
	this->connected = false;
	this->name = name;
	this->portName = portName;
	this->dcbSerial = dcbSerial;

	this->hSerial = CreateFile(portName,
		GENERIC_READ | GENERIC_WRITE,
		0,
		NULL,
		OPEN_EXISTING,
		FILE_ATTRIBUTE_NORMAL,
		NULL);

	//Check if the connection was successfull
	if (this->hSerial == INVALID_HANDLE_VALUE)
	{
		//If not success full display an Error
		if (GetLastError() == ERROR_FILE_NOT_FOUND) {

			//Print Error if neccessary
			cout << "ERROR: Handle was not attached. Reason: " << name << " not available.\n" << endl;

		}
		else
		{
			printf("ERROR!!!\n");
		}
	}
	else
	{
		DCB dcbSerialBuff = { 0 };
		//Try to get the current
		if (!GetCommState(this->hSerial, &dcbSerialBuff))
		{
			//If impossible, show an error
			printf("failed to get current serial parameters!\n");
		}
		else
		{
			//Set the parameters and check for their proper application
			if (!SetCommState(hSerial, &dcbSerial))
			{
				printf("ALERT: Could not set Serial Port parameters\n");
			}
			else
			{
				//If everything went fine we're connected
				cout << name << " connected\n" << endl;
				this->connected = true;
				//Flush any remaining characters in the buffers 
				PurgeComm(this->hSerial, PURGE_RXCLEAR | PURGE_TXCLEAR);
			}
		}
	}

}

KSerial::~KSerial()
{
	if (this->connected)
	{
		//We're no longer connected
		this->connected = false;
		//Close the serial handler
		CloseHandle(this->hSerial);
	}
}

int KSerial::ReadData(char * buffer)
{
	//Number of bytes we'll have read
	DWORD bytesRead;

	//Use the ClearCommError function to get status info on the Serial port
	ClearCommError(this->hSerial, &this->errors, &this->status);


	//Check if there is something to read
	if (this->status.cbInQue > 0)
	{
		//Try to read the require number of chars, and return the number of read bytes on success
		if (ReadFile(this->hSerial, buffer, 1, &bytesRead, NULL))
		{
			return bytesRead;
		}
	}
	Sleep(1);
	//If nothing has been read, or that an error was detected return 0
	return 0;
}

bool KSerial::WriteData(const char * buffer, unsigned int nbChar)
{
	DWORD bytesSend;


	//Try to write the buffer on the Serial port
	if (!WriteFile(this->hSerial, (void *)buffer, nbChar, &bytesSend, 0))
	{
		//In case it don't work get comm error and return false
		ClearCommError(this->hSerial, &this->errors, &this->status);
		KUtils::write_text_to_log_file("[error]" + string(buffer), this->name + "_serial", true, true);
		return false;
	}
	else
	{
		KUtils::write_text_to_log_file("[sent]" + string(buffer), this->name + "_serial", true, true);
		return true;
	}
}

bool KSerial::IsConnected()
{
	return this->connected;
}

