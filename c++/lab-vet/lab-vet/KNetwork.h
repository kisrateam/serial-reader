#include <WinSock2.h>
#include <WS2tcpip.h>
#include <system_error>
#include <string>
#include <iostream>


using namespace std;
#define DEFAULT_BUFLEN 20480
#define MAX_CLIENTS 50

#pragma comment(lib,"ws2_32.lib")
class KNetwork
{
public:
	// import code
	char recvbuf[DEFAULT_BUFLEN] = "";
	int len, receiveres, clients_connected = 0;
	bool active = TRUE;
	SOCKET server_socket = INVALID_SOCKET;
	SOCKET client_fd;
	sockaddr_in server;
	struct _clients_b {
		bool connected;
		SOCKET ss;
	};
	_clients_b clients[MAX_CLIENTS];


	KNetwork(string name, int port, string protocal);
	void initTCP(int port);
	void start_server(int port);
	void initUDP(int port);
	~KNetwork();

	void SendTo(const std::string& address, unsigned short port, const char* buffer, int len, int flags);
	void SendTo(sockaddr_in& address, const char* buffer, int len, int flags);
	sockaddr_in RecvFrom(char* buffer, int len, int flags);
	int Receive(SOCKET new_socket, char * buffer);
	void SetOption(int opt, int flag);
	void Bind(unsigned short port);
	void Listen();

	SOCKET Accept();

private:
	WSAData data;
	SOCKET sock;

	sockaddr_in add;
};

