#include "KDevice.h"

wstring s2ws(const string& s)
{
	int len;
	int slength = (int)s.length() + 1;
	len = MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, 0, 0);
	wchar_t* buf = new wchar_t[len];
	MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, buf, len);
	wstring r(buf);
	delete[] buf;
	return r;
}

KDevice::KDevice(string name, string comport, int buadrate)
{
	cout << name << " Serial is Started" << endl;

	wstring comPrefix = L"\\\\.\\";
	wstring comID = comPrefix + s2ws(comport);
	LPCWSTR comPort = comID.c_str();
	

	DCB dcbSerial = { 0 };
	dcbSerial.BaudRate = buadrate;
	dcbSerial.ByteSize = 8;
	dcbSerial.StopBits = ONESTOPBIT;
	dcbSerial.Parity = NOPARITY;
	dcbSerial.fDtrControl = DTR_CONTROL_ENABLE;

	this->name = name;
	this->SP = new KSerial(name, comPort, dcbSerial);
	this->api = new KAPI();
	this->jsonObj = new KJson();
}



KDevice::KDevice(string name, int port, string protocol)
{
	cout << name << " Network TCP is Started" << endl;
	this->name = name;
	this->net = new KNetwork(name, port, protocol);	
	this->api = new KAPI();
	this->jsonObj = new KJson();
}

KDevice::~KDevice()
{
}

int KDevice::readSerial()
{
	return 0;
}

int KDevice::udpReceive()
{
	return 0;
}

int KDevice::tcpReceive()
{
	return 0;
}

