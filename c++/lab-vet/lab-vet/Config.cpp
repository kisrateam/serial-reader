#include "Config.h"


Config::Config()
{
	std::ifstream cFile("Config.txt");
	if (cFile.is_open())
	{
		std::string line;
		while (getline(cFile, line)) {
			line.erase(std::remove_if(line.begin(), line.end(), isspace),
				line.end());
			if (line[0] == '#' || line.empty())
				continue;
			auto delimiterValue = line.find("=");
			auto delimiterSuffix = line.find(".");
			auto prefix = line.substr(0, delimiterSuffix);
			auto suffix = line.substr(delimiterSuffix + 1, delimiterValue - delimiterSuffix - 1);
			auto value = line.substr(delimiterValue + 1);

			if (prefix.compare("MYTHIC18") == 0)
			{
				this->Mythic18.name = prefix;

				if (suffix.compare("PORT") == 0)
				{
					this->Mythic18.localPort = stoi(value);
				}
			}
			if (prefix.compare("BC5000") == 0)
			{
				this->BC5000.name = prefix;

				if (suffix.compare("PORT") == 0)
				{
					this->BC5000.localPort = stoi(value);
				}
			}
			if (prefix.compare("DYMIND") == 0)
			{
				this->Dymind.name = prefix;

				if (suffix.compare("PORT") == 0)
				{
					this->Dymind.localPort = stoi(value);
				}
			}
			else if (prefix.compare("C600") == 0)
			{
				setCom(&this->C600, prefix, suffix, value);
			}
			else if (prefix.compare("C380") == 0)
			{
				setCom(&this->C380, prefix, suffix, value);
			}
			else if (prefix.compare("ISE4000") == 0)
			{
				setCom(&this->ISE4000, prefix, suffix, value);
			}
			else if (prefix.compare("URIT500C") == 0)
			{
				setCom(&this->URIT500C, prefix, suffix, value);
			}
			else if (prefix.compare("HOST") == 0)
			{
				this->HostName = value;
			}
			else if (prefix.compare("TIMEOUT") == 0)
			{
				this->timeout = stol(value);
			}
		}
	}
	else {
		std::cerr << "Couldn't open config file for reading.\n";
	}
}


Config::~Config()
{
}

void Config::setCom(Serial *serial, string prefix, string suffix, string value)
{
	serial->name = prefix;

	if (suffix.compare("COM") == 0)
	{
		serial->comPort = value;
	}
	else if (suffix.compare("BUADRATE") == 0)
	{
		serial->buadRate = stoi(value);
	}
}
