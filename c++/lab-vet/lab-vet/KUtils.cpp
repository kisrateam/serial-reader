#include "KUtils.h"

vector<string> KUtils::split(string str, char delimiter) {
	vector<string> internal;
	stringstream ss(str); // Turn the string into a stream.
	string tok;
	try
	{
		while (getline(ss, tok, delimiter)) {
			internal.push_back(tok);
		}
		internal.push_back("");

		return internal;
	}
	catch (const std::exception& e)
	{
		KUtils::write_text_to_log_file("split error\n" + string(e.what()), "KUtils", true, true);
	}
}

vector<string> KUtils::splitWord(string str) {
	vector<string> internal;
	stringstream ss(str); // Turn the string into a stream.
	string temp;
	try
	{
		while (!ss.eof()) {
			ss >> temp;
			internal.push_back(temp);
			temp = "";
		}
		internal.push_back("");

		return internal;
	}
	catch (const std::exception& e)
	{
		KUtils::write_text_to_log_file("splitWord error\n" + string(e.what()), "KUtils", true, true);
	}
}

string KUtils::setBarcodeFormat(string barcode, int position)
{
	try
	{
		int sum = barcode.size() - position;
		if (sum >= 0)
		{
			return barcode.substr(barcode.size() - position);
		}
		else
		{
			return barcode;
		}
	}
	catch (std::exception& e) {
		KUtils::write_text_to_log_file("setBarcodeFormat error\n" + string(e.what()), "KUtils", true, true);
	}
}

string KUtils::utf8_to_string(const char *utf8str, const locale& loc) {
	try
	{
		// UTF-8 to wstring
		wstring_convert<codecvt_utf8<wchar_t>> wconv;
		wstring wstr = wconv.from_bytes(utf8str);
		// wstring to string
		vector<char> buf(wstr.size());
		use_facet<ctype<wchar_t>>(loc).narrow(wstr.data(), wstr.data() + wstr.size(), '?', buf.data());
		return string(buf.data(), buf.size());
	}
	catch (std::exception& e) {
		KUtils::write_text_to_log_file("utf8_to_string error\n" + string(e.what()), "KUtils", true, true);
	}
}

void KUtils::write_text_to_log_file(const std::string &text, string filename, bool date, bool endline)
{

	time_t now = time(0);
	tm *ltm = localtime(&now);
	const char* dt = ctime(&now);
	dt = asctime(ltm);
	string str = string(dt);
	string date_time = str.substr(0, str.length() - 1);
	string name_with_date = filename + "_" + to_string(1900 + ltm->tm_year) + to_string(1 + ltm->tm_mon) + to_string(ltm->tm_mday) + ".txt";
	std::ofstream log_file(name_with_date, std::ios_base::out | std::ios_base::app);
	try {

		string date_s = "[" + date_time + "]";


		cout << (date ? date_s : "") << text << (endline ? "\n" : "");
		log_file << (date ? date_s : "") << text << (endline ? "\n" : "");
	}
	catch (std::exception& e) {
		log_file << "[" << date_time << "]" << "write_text_to_log_file error" << endl;
	}

}