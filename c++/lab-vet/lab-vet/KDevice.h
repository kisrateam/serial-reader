#pragma once

#include "KNetwork.h"
#include "KSerial.h"
#include "KJson.h"
#include "KAPI.h"
#include "KUtils.h"
#include <algorithm>

constexpr auto STX = 0x02;
constexpr auto ETX = 0x03;
constexpr auto EOT = 0x04;
constexpr auto ENQ = 0x05;
constexpr auto ACK = 0x06;
constexpr auto NAK = 0x15;
constexpr auto ETB = 0x17;

constexpr auto CR = 0x0D;
constexpr auto LF = 0x0A;

class KDevice
{
public:
	KDevice(string name, string comport, int buadrate);
	KDevice(string name, int port, string protocol);
	~KDevice();

	string name;
	KSerial* SP;
	KNetwork* net;
	KAPI* api;
	KJson *jsonObj;
	virtual int readSerial();
	virtual int udpReceive();
	virtual int tcpReceive();
};

