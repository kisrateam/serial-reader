#pragma once
#include <iostream>
#include <curl.h>
#include <string>

using namespace std;

class KAPI
{
public:
	KAPI();
	~KAPI();
	CURLcode resultRawData();
	CURLcode resultRawData2();
	CURLcode requestCS400(string barcode, string * response);

	struct curl_slist *slist1;
	string jsonData;
};