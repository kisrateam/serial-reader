#include "KJson.h"
#include "KUtils.h"


KJson::KJson()
{
}


KJson::~KJson()
{
}

string KJson::addOrder(string barcode, string dateTime, string deviceName, string mode, string type, string test)
{
	string order = "{\"barcode\":\"" + barcode + "\",\"datetime\":\"" + dateTime + "\",\"device_name\":\"" + deviceName + "\",\"mode\":\"" + mode + "\",\"type\":\"" + type + "\",\"test\":\"" + test + "\",\"results\":[";
	//KUtils::write_text_to_log_file("order : "+ order + "\n", "BS600M", true, true);
	this->jsonData += order;
	return order;
}

string KJson::addOrderBS620(string barcode, string dateTime, string deviceName, string mode, string type, string test)
{
	string order = "{\"barcode\":\"" + barcode + "\",\"datetime\":\"" + dateTime + "\",\"device_name\":\"" + deviceName + "\",\"mode\":\"" + mode + "\",\"type\":\"" + type + "\",\"test\":\"" + test + "\",\"results\":[";
	//KUtils::write_text_to_log_file("order : "+ order + "\n", "BS600M", true, true);
	this->jsonData = order;
	return order;
}

string KJson::addResult(string test, string value, string unit, string flag, string lowPanic, string low, string high, string highPanic)
{
	string result = "{\"test\":\"" + test + "\",\"value\":\"" + value + "\",\"unit\":\"" + unit + "\",\"flag\":\"" + flag + "\",\"low_panic\":\"" + lowPanic + "\",\"low\":\"" + low + "\",\"high\":\"" + high + "\",\"high_panic\":\"" + highPanic + "\" },";
	//KUtils::write_text_to_log_file("result : "+result+"\n", "BS600M", true, true);
	this->jsonData += result;
	return result;
}

string KJson::addResultBS620(string test, string value, string unit, string flag, string lowPanic, string low, string high, string highPanic)
{
	string result = "{\"test\":\"" + test + "\",\"value\":\"" + value + "\",\"unit\":\"" + unit + "\",\"flag\":\"" + flag + "\",\"low_panic\":\"" + lowPanic + "\",\"low\":\"" + low + "\",\"high\":\"" + high + "\",\"high_panic\":\"" + highPanic + "\" },";
	//KUtils::write_text_to_log_file("result : "+result+"\n", "BS600M", true, true);
	this->jsonData += result;
	return result;
}

void KJson::jsonDecodeCS400(string str)
{
	try
	{
		auto js = json::parse(str);
	
		cs400Request.number = js["number"].get<string>();
		cs400Request.name = js["name"].get<string>();
		cs400Request.sex = js["sex"].get<string>();
		cs400Request.age = js["age"].get<string>();
		cs400Request.age_unit = js["age_unit"].get<string>();
		cs400Request.barcode = js["barcode"].get<string>();
		cs400Request.orders = js["orders"].get<std::vector<string>>();
	}
	catch (const std::exception&)
	{
		cout << "JSON Data Error" << endl;
	}	
}

void KJson::finish()
{
	try {
		if (this->jsonData.length() > 0)
		{
			this->jsonData.replace(this->jsonData.length() - 1, 2, "]}");
		}
	}
	catch (const std::exception& e)
	{
		cout << "Finish JSON Object error" << e.what() << endl;
	}
	
}

void KJson::clear()
{
	this->jsonData = "";
}
