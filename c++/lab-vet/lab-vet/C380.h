#include "KDevice.h"
#include <ctime>
#include "KJson.h"


class C380 : public KDevice
{
public:
	C380(string comport, int buadrate) : KDevice("C380", comport, buadrate) {	}
	~C380();

	int readSerial();
	void clearBuffer(char  packetBuff[1024], char  packetData[1024]);
	void ack();

private:

	const char pipe = '|';
	const char caret = '^';
	const char back_slash = '\\';
	string delimiterDef = "|\\^&";

	string CheckSum(string s);
	string Packaging(string record);
	string HeaderPacket(int recordNumber);
	string PatientInformationPacket(int recordNumber, int sequenceNumber, string barcode, string name, string sex, string age, string age_unit);
	string TestOrderPacket(int recordNumber, int sequenceNumber, string sample_id, string sample_no, string disk_id, string position_no, string diluent, string universal_test, string piority, string specimen_descriptor, string report_type);
	string TerminatorPacket(int recordNumber, int sequenceNumber, string termination_code);
	void write(KSerial* sp, string str);
};

void defaultRequest(KJson::CS400Request& data);
