#include "KDevice.h"
#include <ctime>
#include "KJson.h"

#pragma once
//class _MSH
//{
//public:
//	string	FieldSeparator;
//	string	EncodingCharacters;
//	string	Sendingapplication;
//	string	SendingFacility;
//	string	DateTimeOfMessage;
//	string	MessageType;
//	string	MessageControlID;
//	string	ProcessingID;
//	string	VersionID;
//	string	CharacterSet;
//};
//
//class _PID
//{
//public:
//	string	SetIDPID;
//	string	PatientIdentifierList;
//	string	PatientName;
//	string	DateTimeofBirth;
//	string	Sex;
//};
//
//class _PV1
//{
//public:
//	string	SetIDPV1;
//	string	PatientClass;
//	string	AssignedPatientLocation;
//	string	FinancialClass;
//};
//
//class _OBR
//{
//public:
//	string	SetIDOBR;
//	string	PlacerOrderNumber;
//	string	FillerOrderNumber;
//	string	UniversalServiceID;
//	string	RequestedDatetime;
//	string	ObservationDateTime;
//	string	CollectorIdentifier;
//	string	RelevantClinicalInfo;
//	string	SpecimenReceivedDateTime;
//	string	SpecimenSource;
//	string	ResultsRptStatusChngDateTime;
//	string	DiagnosticServSectID;
//	string	ResultCopiesTo;
//	string	PrincipalResultInterpreter;
//};
//
//class _OBX
//{
//public:
//	string	SetIDOBX;
//	string	ValueType;
//	string	ObservationIdentifier;
//	string	ObservationValue;
//	string	Units;
//	string	ReferencesRange;
//	string	AbnormalFlags;
//	string	ObservResultStatus;
//
//};

class BS620Mlan : public KDevice
{
public:
	BS620Mlan(int port) : KDevice("BS620Mlan", port, "TCP") {}
	~BS620Mlan();

	const char pipe = '|';
	const char caret = '^';
	const char back_slash = '\\';
	string delimiterDef = "|\\^&";
	int tcpReceive();
	void decodeProcess();
	char* getIP();
	//_MSH decodeMSH(string field);
	//_PID decodePID(string field);
	//_PV1 decodePV1(string field);
	//_OBR decodeOBR(string field);
	//_OBX decodeOBX(string field);
};
