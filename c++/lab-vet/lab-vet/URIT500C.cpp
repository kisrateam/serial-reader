#include "URIT500C.h"

URIT500C::~URIT500C()
{

}

int URIT500C::readSerial()
{
	char c = 0;
	char packetBuff[1024] = "";
	char packetData[1024] = "";
	int packetLength = 0;
	int readResult = 0;
	int state = 0;
	string barcode;

	while (this->SP->IsConnected())
	{
		try
		{
			readResult = this->SP->ReadData(&c);
		}
		catch (const std::exception& e)
		{
			KUtils::write_text_to_log_file("read serial\n" + string(e.what()), this->name, true, true);
		}


		if (readResult > 0)
		{
			string s(1, c);
			KUtils::write_text_to_log_file(s, this->name, false, false);
			if (c == STX)
			{
				packetLength = 0;
				state = 1;
			}

			if (state)
			{
				packetBuff[packetLength] = c;
				packetLength++;

				if (c == ETX)
				{
					try
					{
						memcpy(packetData, packetBuff, packetLength);
					}
					catch (const std::exception& e)
					{
						KUtils::write_text_to_log_file("copy packet\n" + string(e.what()), this->name, true, true);
					}

					vector<string> record;
					record = KUtils::split(packetData, 0x0A);

					for (int i = 0; i < (int)record.size(); i++)
					{

						vector<string> data;
						vector<string> rec1;
						vector<string> timeRecord;

						data = KUtils::splitWord(record[i]);
						string test(data[0]);
						string value;
						if (data[1] == "+") {
							value = "Positive";
						}
						else if (data[1] == "-") {
							value = "Negative";
						}
						else if (data[1] == "+-") {
							value = "Trace";
						}
						else {
							value = data[1];
						}



						if (data[0].size() > 0)
						{
							test.erase(remove(test.begin(), test.end(), '*'), test.end());
						}

						if (data[0].substr(0, 2) == "ID")
						{
							barcode = data[0].substr(3, 10);
						}
						if (data[0].substr(0, 2) == "NO")
						{
							try
							{
								timeRecord = KUtils::splitWord(record[i + 1]);
								string date(data[1]);
								date.erase(remove(date.begin(), date.end(), '-'), date.end());
								string time(timeRecord[0]);
								time.erase(remove(time.begin(), time.end(), ':'), time.end());

								string dateTime = date + time;

								this->jsonObj->addOrder(barcode, dateTime, this->name, "", "", "");
							}
							catch (const std::exception& e)
							{
								KUtils::write_text_to_log_file("Add order error\n" + string(e.what()), this->name, true, true);
							}
						}
						else if (test == "WBC")
						{
							this->jsonObj->addResult(test, value, "", (record[i][0] == '*') ? "A" : "N", "", "", "", "");
						}
						else if (test == "pH")
						{
							this->jsonObj->addResult(test, value, "", (record[i][0] == '*') ? "A" : "N", "", "", "", "");
						}
						else if (test == "SG")
						{
							this->jsonObj->addResult(test, value, "", (record[i][0] == '*') ? "A" : "N", "", "", "", "");
						}
						else if (test == "PRO")
						{
							this->jsonObj->addResult(test, value, "", (record[i][0] == '*') ? "A" : "N", "", "", "", "");
						}
						else if (test == "GLU")
						{
							this->jsonObj->addResult(test, value, "", (record[i][0] == '*') ? "A" : "N", "", "", "", "");
						}
						else if (test == "KET")
						{
							this->jsonObj->addResult(test, value, "", (record[i][0] == '*') ? "A" : "N", "", "", "", "");
						}
						else if (test == "BLD")
						{
							this->jsonObj->addResult(test, value, "", (record[i][0] == '*') ? "A" : "N", "", "", "", "");
						}
						else if (test == "BIL")
						{
							this->jsonObj->addResult(test, value, "", (record[i][0] == '*') ? "A" : "N", "", "", "", "");
						}
						else if (test == "LEU")
						{
							this->jsonObj->addResult(test, value, "", (record[i][0] == '*') ? "A" : "N", "", "", "", "");
						}
						else if (test == "NIT")
						{
							this->jsonObj->addResult(test, value, "", (record[i][0] == '*') ? "A" : "N", "", "", "", "");
						}
						else if (test == "URO")
						{
							this->jsonObj->addResult(test, value, "", (record[i][0] == '*') ? "A" : "N", "", "", "", "");
						}
						else if (test == "CR")
						{
							makeValue(value);

							this->jsonObj->addResult(test, value, "", (record[i][0] == '*') ? "A" : "N", "", "", "", "");
						}
						else if (test == "Ca")
						{
							makeValue(value);

							this->jsonObj->addResult(test, value, "", (record[i][0] == '*') ? "A" : "N", "", "", "", "");
						}
						else if (test == "MA")
						{
							makeValue(value);

							this->jsonObj->addResult(test, value, "", (record[i][0] == '*') ? "A" : "N", "", "", "", "");
						}
						else if (record[i][0] == ETX)
						{
							try
							{
								this->jsonObj->finish();
								this->api->jsonData = this->jsonObj->jsonData;
								cout << "Response = " << this->api->resultRawData() << endl;
								jsonObj->clear();
								memset(packetBuff, 0, sizeof packetBuff);
								packetLength = 0;
								state = 0;
							}
							catch (const std::exception& e)
							{
								KUtils::write_text_to_log_file("Finish json obj error\n" + string(e.what()), this->name, true, true);

							}

						}
					}
				}
			}

		}

	}
	return 0;
}

void URIT500C::makeValue(std::string &value)
{
	value.erase(remove(value.begin(), value.end(), '/'), value.end());
	value.erase(remove_if(value.begin(), value.end(), &isalpha), value.end());
}

void URIT500C::makeUnit(std::string &unit)
{
	unit.erase(remove(unit.begin(), unit.end(), '<'), unit.end());
	unit.erase(remove(unit.begin(), unit.end(), '='), unit.end());
	unit.erase(remove(unit.begin(), unit.end(), '>'), unit.end());
	unit.erase(remove(unit.begin(), unit.end(), '.'), unit.end());
	unit.erase(remove_if(unit.begin(), unit.end(), &isdigit), unit.end());
}