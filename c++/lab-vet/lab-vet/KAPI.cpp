#include "KAPI.h"
#include "KUtils.h"
#include "Config.h"
#include <thread>

KAPI::KAPI()
{
	this->slist1 = NULL;
	this->slist1 = curl_slist_append(this->slist1, "Content-Type: application/json");


}


KAPI::~KAPI()
{
}

namespace
{
	std::size_t callback(
		const char* in,
		std::size_t size,
		std::size_t num,
		std::string* out)
	{
		const std::size_t totalBytes(size * num);
		out->append(in, totalBytes);
		return totalBytes;
	}
}

size_t WriteCallback(char *contents, size_t size, size_t nmemb, void *userp)
{
	((std::string*)userp)->append((char*)contents, size * nmemb);
	return size * nmemb;
}

CURLcode KAPI::resultRawData()
{
	CURL *curl;
	CURLcode res;
	string readBuffer;
	curl = curl_easy_init();
	Config* config = config->getInstance();
	string url = "http://" + config->HostName + "/api/result-raw-data";

	if (curl)
	{
		KUtils::write_text_to_log_file(this->jsonData, "post_data", true, true);

		curl_easy_setopt(curl, CURLOPT_URL, url);
		curl_easy_setopt(curl, CURLOPT_NOPROGRESS, 1L);
		curl_easy_setopt(curl, CURLOPT_POSTFIELDS, this->jsonData.c_str());
		curl_easy_setopt(curl, CURLOPT_POSTFIELDSIZE, this->jsonData.length());
		curl_easy_setopt(curl, CURLOPT_USERAGENT, "curl/7.38.0");
		curl_easy_setopt(curl, CURLOPT_HTTPHEADER, this->slist1);
		curl_easy_setopt(curl, CURLOPT_MAXREDIRS, 50L);
		curl_easy_setopt(curl, CURLOPT_TIMEOUT, config->timeout);
		curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "POST");
		curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteCallback); // check response
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, &readBuffer);

		//curl_easy_setopt(curl, CURLOPT_NOBODY, 1); //dont print out??
	}
	//std::this_thread::sleep_for(200ms);

	res = curl_easy_perform(curl);
	KUtils::write_text_to_log_file(readBuffer, "response", true, true);

	curl_easy_cleanup(curl);

	return res;
}

CURLcode KAPI::resultRawData2()
{
	CURL *curl;
	CURLcode res;
	string readBuffer;
	curl = curl_easy_init();
	Config* config = config->getInstance();
	string url = "http://" + config->HostName + "/api/result-raw-data";

	if (curl)
	{
		KUtils::write_text_to_log_file(this->jsonData, "post_data", true, true);

		curl_easy_setopt(curl, CURLOPT_URL, url);
		curl_easy_setopt(curl, CURLOPT_NOPROGRESS, 1L);
		curl_easy_setopt(curl, CURLOPT_POSTFIELDS, this->jsonData.c_str());
		curl_easy_setopt(curl, CURLOPT_POSTFIELDSIZE, this->jsonData.length());
		curl_easy_setopt(curl, CURLOPT_USERAGENT, "curl/7.38.0");
		curl_easy_setopt(curl, CURLOPT_HTTPHEADER, this->slist1);
		curl_easy_setopt(curl, CURLOPT_MAXREDIRS, 50L);
		curl_easy_setopt(curl, CURLOPT_TIMEOUT, config->timeout);
		curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "POST");
		curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteCallback); // check response
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, &readBuffer);

		//curl_easy_setopt(curl, CURLOPT_NOBODY, 1); //dont print out??
	}
	//std::this_thread::sleep_for(200ms);

	res = curl_easy_perform(curl);
	KUtils::write_text_to_log_file(readBuffer, "response", true, true);

	curl_easy_cleanup(curl);

	return res;
}

CURLcode KAPI::requestCS400(string barcode, string *response)
{
	CURL *curl;
	CURLcode res;
	Config* config = config->getInstance();

	curl = curl_easy_init();

	string url = "http://" + config->HostName + "/api/request-cs400/barcode/" + barcode;
	//string url = "http://vet02.dyndns.org:81/api/request-cs400/barcode/" + barcode;


	if (curl)
	{
		curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
		curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, callback);
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, response);
		curl_easy_setopt(curl, CURLOPT_TIMEOUT, config->timeout);
	}
	res = curl_easy_perform(curl);
	curl_easy_cleanup(curl);
	cout << *response << endl;
	//KUtils::write_text_to_log_file(*httpData.get(), "reponse", true, true);

	return res;
}
