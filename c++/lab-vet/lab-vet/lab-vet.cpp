#include <iostream>
#include "CS400.h"
#include "C380.h"
#include "C380-lan.h"
#include "ISE4000.h"
#include "URIT500C.h"
#include "Mythic18.h"
#include "Dymine.h"
#include "KJson.h"
#include "KAPI.h"
#include <sstream>
#include <thread>
#include "Config.h"
#include "BC5000.h"


Config *Config::instance = 0;

int main()
{
	Config* config = config->getInstance();

	//KDevice* cs400 = new CS400(config->C600.comPort, config->C600.buadRate);
	//KDevice* c380 = new C380(config->C380.comPort, config->C380.buadRate);
	//KDevice* ise4000 = new ISE4000(config->ISE4000.comPort, config->ISE4000.buadRate);
	//KDevice* urit500c = new URIT500C(config->URIT500C.comPort, config->URIT500C.buadRate);
	//KDevice* mythic18 = new Mythic18(config->Mythic18.localPort);
	//KDevice* dymine = new Dymine(config->Dymind.localPort);
	KDevice* bc5000 = new BC5000(config->BC5000.localPort);
	//KDevice* bs620mlan = new BS620Mlan(config->BS620Mlan.localPort);

	//thread cs400_t(&KDevice::readSerial, cs400);
	//thread c380_t(&KDevice::readSerial, c380);
	//thread ise4000_t(&KDevice::readSerial, ise4000);
	//thread urit500c_t(&KDevice::readSerial, urit500c);
	//thread mythic18_t(&KDevice::udpReceive, mythic18);
	//thread dymine_t(&KDevice::tcpReceive, dymine);
	thread bc5000_t(&KDevice::tcpReceive, bc5000);
	//thread bs620mlan_t(&KDevice::tcpReceive, bs620mlan);

	//cs400_t.join();
	//c380_t.join();
	//ise4000_t.join();
	//urit500c_t.join();
	//mythic18_t.join();
	//dymine_t.join();
	bc5000_t.join();
	//bs620mlan_t.join();

	//while (true);

	return 0;
}
