#include <iostream>
#include <fstream>
#include <algorithm>
#include <string>
using namespace std;

class Config
{
public:
	Config();
	~Config();
	struct Serial
	{
		string comPort;
		unsigned int buadRate;
		string name;
	};
	struct UDP
	{
		unsigned int localPort;
		string name;
	};

	struct TCP
	{
		unsigned int localPort;
		string name;
	};
	void setCom(Serial *serial, string prefix, string suffix, string value);

	Serial ISE4000;
	Serial URIT500C;
	Serial C600;
	Serial C380;
	UDP Mythic18;
	TCP Dymind;
	TCP BC5000;
	TCP BS620Mlan;
	string HostName;
	long timeout;
	static Config *instance;

public:
	static Config *getInstance() {
		if (!instance)
			instance = new Config;
		return instance;
	}
};

