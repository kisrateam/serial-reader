#include "Mythic18.h"

Mythic18::~Mythic18()
{
}

int Mythic18::udpReceive()
{
	char buff[4096];

	string barcode;
	string date;
	string time;
	string mode;
	string type;
	string test;
	vector<string> record;
	vector<string> column;
	string header;

	try
	{
		while (true)
		{
			try
			{
				sockaddr_in add = this->net->RecvFrom(buff, sizeof(buff), 0);
				KUtils::write_text_to_log_file(buff, this->name, true, true);
			}
			catch (const std::exception& e)
			{
				KUtils::write_text_to_log_file("Recieve udp\n" + string(e.what()), this->name, true, true);
			}

			record = KUtils::split(buff, 0x0D);

			try
			{
				header = record[0].substr(0, 6);
			}
			catch (const std::exception& e)
			{
				KUtils::write_text_to_log_file("sub string header\n" + string(e.what()), this->name, true, true);
			}

			if (!string("MYTHIC").compare(header))
			{
				for (int i = 0; i < (int)record.size(); i++)
				{

					column = KUtils::split(record[i], ';');

					if (i == MYTHIC)
					{
						if (column[2] != "RESULT")
						{
							jsonObj->clear();
							memset(buff, 0, sizeof buff);
							break;
						}
					}
					else if (i == DATE)
					{
						//date.erase(remove(date.begin(), date.end(), '/'), date.end());
						try
						{
							vector<string> dateBuff = KUtils::split(column[1], '/');
							date = dateBuff[2] + dateBuff[1] + dateBuff[0];
						}
						catch (const std::exception& e)
						{
							KUtils::write_text_to_log_file("DATE\n" + string(e.what()), this->name, true, true);
						}


					}
					else if (i == TIME)
					{
						try
						{
							time = column[1];
							time.erase(remove(time.begin(), time.end(), ':'), time.end());
						}
						catch (const std::exception& e)
						{
							KUtils::write_text_to_log_file("TIME\n" + string(e.what()), this->name, true, true);
						}

					}
					else if (i == MODE)
					{
						mode = column[1];
					}
					else if (i == UNIT)
					{

					}
					else if (i == SEQ)
					{

					}
					else if (i == SID)
					{

					}
					else if (i == PID)
					{

					}
					else if (i == ID)
					{
						barcode = KUtils::setBarcodeFormat(column[1], 10);
					}
					else if (i == TYPE)
					{
						type = column[1];
					}
					else if (i == TEST)
					{
						test = column[1];

						this->jsonObj->addOrder(barcode, date + time, this->name, mode, type, test);
					}
					else if (i == OPERATOR)
					{

					}
					else if (i >= WBC && i <= PDW)
					{
						try
						{
							for (int j = 0; j < (int)column.size(); j++)
								column[j].erase(remove(column[j].begin(), column[j].end(), ' '), column[j].end());

							this->jsonObj->addResult(column[0], column[1], "", column[3], column[4], column[5], column[6], column[7]);
						}
						catch (const std::exception& e)
						{
							KUtils::write_text_to_log_file("WBC >= =< PDW\n" + string(e.what()), this->name, true, true);
						}

					}
					else if (i == WBCconst)
					{

					}
				}

				try
				{
					this->jsonObj->finish();
					this->api->jsonData = this->jsonObj->jsonData;
					cout << "Response = " << this->api->resultRawData() << endl;
					jsonObj->clear();
					memset(buff, 0, sizeof buff);
				}
				catch (const std::exception& e)
				{
					KUtils::write_text_to_log_file("Finish json obj error\n" + string(e.what()), this->name, true, true);
				}
			}
			else
			{
				memset(buff, 0, sizeof buff);
			}
		}
	}
	catch (std::system_error& e)
	{
		KUtils::write_text_to_log_file("system_error\n" + string(e.what()), this->name, true, true);
	}


	return 0;
}
