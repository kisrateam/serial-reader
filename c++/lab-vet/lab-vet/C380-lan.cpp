#include "C380-lan.h"
#include <iostream>
#include <cstring>
#include <winsock2.h>
#include <sys/types.h>

#pragma comment(lib, "ws2_32.lib")
#pragma warning(disable:4996) 

BS620Mlan::~BS620Mlan()
{

}


int BS620Mlan::tcpReceive()
{
//	while (net->active) {
//		//start accepting clients ------------------------------
//		net->len = sizeof(net->server);
//		net->client_fd = accept(net->server_socket, (struct sockaddr*)&net->server, &net->len);
//		//our client is a real thing?
//		if (net->client_fd != INVALID_SOCKET) {
//			//save client socket into our struct table
//			string ip = getIP();
//			net->clients[net->clients_connected].ss = net->client_fd;
//			net->clients[net->clients_connected].connected = TRUE;
//			//and of course we need a calculator too
//			net->clients_connected++;
//			cout << "New client: " << net->client_fd << " IP: " << ip << endl;
//		}
//		//we might need to add some delays cause our code might be too fast
//		//commenting this function will eat your cpu like the hungriest dog ever
//
//			//plus we don't need to loop that fast anyways
//		Sleep(1);
//		//receiving and sending data ---------------------------
//		//we have clients or no?
//		if (net->clients_connected > 0) {
//			//lets go through all our clients
//			for (int cc = 0; cc < net->clients_connected; cc++) {
//				memset(&net->recvbuf, 0, sizeof(net->recvbuf));
//				if (net->clients[cc].connected) {
//
//					//receive data
//					net->receiveres = recv(net->clients[cc].ss, net->recvbuf,
//
//						DEFAULT_BUFLEN, 0);
//
//					//and echo it back if we get any
//					if (net->receiveres > 0) {
//						//Sleep(10000);
//						KUtils::write_text_to_log_file(net->recvbuf, this->name, true, true);
//						//decodeProcess();
//					}
//					//how to close connection
//					//this just for quick example
//					//so you are just getting rid off client's socket data
//
//					else if (net->receiveres == 0 || strcmp(net->recvbuf,
//
//						"disconnect") == 0) {
//
//						cout << "Client disconnected." << endl;
//						net->clients[cc].connected = FALSE;
//						net->clients_connected--;
//						//delete [cc] clients;
//					}
//				}
//			}
//		}
//	}
//	//when we shut down our server
//	closesocket(net->server_socket);
//	// Clean up winsock
//	WSACleanup();
	return 0;
}
//
//
//char* BS620Mlan::getIP()
//{
//	char* s = NULL;
//	s = (char*)malloc(INET_ADDRSTRLEN);
//	inet_ntop(AF_INET, &(net->server.sin_addr), s, INET_ADDRSTRLEN);
//	return s;
//}
//
//void BS620Mlan::decodeProcess() {
//
//	int packetLength = strlen(net->recvbuf);
//	vector<string> record;
//	vector<string> column;
//	_MSH msh;
//	_PID pid;
//	_PV1 pv1;
//	_OBR obr;
//	_OBX obx;
//
//	if (net->recvbuf[0] == 0x0B && (net->recvbuf[packetLength - 1]) == 0x0D && (net->recvbuf[packetLength - 2]) == 0x1C)
//	{
//		record = KUtils::split(net->recvbuf, 0x0D);
//		for (int i = 0; i < record.size(); i++)
//		{
//			column = KUtils::split(record[i], this->pipe);
//			if (column[0] == "\x0bMSH")
//			{
//				msh = decodeMSH(record[i]);
//			}
//			else if (column[0] == "PID")
//			{
//				pid = decodePID(record[i]);
//
//			}
//			else if (column[0] == "PV1")
//			{
//				//pv1 = decodePV1(record[i]);
//
//			}
//			else if (column[0] == "OBR")
//			{
//				string ip = getIP();
//				obr = decodeOBR(record[i]);
//				jsonObj->addOrder(obr.FillerOrderNumber, msh.DateTimeOfMessage, this->name, obr.PrincipalResultInterpreter, ip, "");
//			}
//			else if (column[0] == "OBX")
//			{
//				obx = decodeOBX(record[i]);
//
//				if (obx.ValueType == "NM")
//				{
//					vector<string> observationIdentifier = KUtils::split(obx.ObservationIdentifier, '^');
//					vector<string> referencesRange = KUtils::split(obx.ReferencesRange, '-');
//					vector<string> abnormalFlags = KUtils::split(obx.AbnormalFlags, '~');
//
//					if (observationIdentifier[1] != "Age")
//					{
//						jsonObj->addResult(observationIdentifier[1], obx.ObservationValue, obx.Units, abnormalFlags[1], "", referencesRange[0], referencesRange[1], "");
//					}
//				}
//
//			}
//			else if (column[0] == "\x1c")
//			{
//				jsonObj->finish();
//				api->jsonData = jsonObj->jsonData;
//				cout << "Response = " << api->resultRawData() << endl;
//				jsonObj->clear();
//				memset(net->recvbuf, 0, sizeof net->recvbuf);
//			}
//		}
//	}
//}
//
//
//_MSH BS620Mlan::decodeMSH(string field)
//{
//	vector<string> mshData = KUtils::split(field, this->pipe);
//	_MSH* msh = new _MSH();
//	if (mshData.size() >= 17) {
//		msh->FieldSeparator = mshData[0];
//		msh->EncodingCharacters = mshData[1];
//		msh->Sendingapplication = mshData[2];
//		msh->SendingFacility = mshData[3];
//		msh->DateTimeOfMessage = mshData[6];
//		msh->MessageType = mshData[8];
//		msh->MessageControlID = mshData[9];
//		msh->ProcessingID = mshData[10];
//		msh->VersionID = mshData[11];
//		msh->CharacterSet = mshData[17];
//	}
//	else
//	{
//		msh = NULL;
//	}
//	return *msh;
//}
//
//_PID BS620Mlan::decodePID(string field)
//{
//	vector<string> pidData = KUtils::split(field, this->pipe);
//	_PID* pid = new _PID();
//	if (pidData.size() >= 8) {
//		pid->SetIDPID = pidData[1];
//		pid->PatientIdentifierList = pidData[3];
//		pid->PatientName = pidData[5];
//		pid->DateTimeofBirth = pidData[7];
//		pid->Sex = pidData[8];
//	}
//	else
//	{
//		pid = NULL;
//	}
//	return *pid;
//}
//
//_PV1 BS620Mlan::decodePV1(string field)
//{
//	vector<string> mshData = KUtils::split(field, this->pipe);
//	_PV1* pv1 = new _PV1();
//	return *pv1;
//}
//
//_OBR BS620Mlan::decodeOBR(string field)
//{
//	vector<string> obrData = KUtils::split(field, this->pipe);
//	_OBR* obr = new _OBR();
//	if (obrData.size() >= 32) {
//		obr->SetIDOBR = obrData[1];
//		obr->PlacerOrderNumber = obrData[2];
//		obr->FillerOrderNumber = obrData[3];
//		obr->UniversalServiceID = obrData[4];
//		obr->RequestedDatetime = obrData[6];
//		obr->ObservationDateTime = obrData[7];
//		obr->CollectorIdentifier = obrData[10];
//		obr->RelevantClinicalInfo = obrData[13];
//		obr->SpecimenReceivedDateTime = obrData[14];
//		obr->SpecimenSource = obrData[15];
//		obr->ResultsRptStatusChngDateTime = obrData[22];
//		obr->DiagnosticServSectID = obrData[24];
//		obr->ResultCopiesTo = obrData[28];
//		obr->PrincipalResultInterpreter = obrData[32];
//	}
//	else
//	{
//		obr = NULL;
//	}
//	return *obr;
//}
//
//_OBX BS620Mlan::decodeOBX(string field)
//{
//
//
//	vector<string> obxData = KUtils::split(field, this->pipe);
//	_OBX* obx = new _OBX();
//	if (obxData.size() >= 11) {
//		obx->SetIDOBX = obxData[1];
//		obx->ValueType = obxData[2];
//		obx->ObservationIdentifier = obxData[3];
//		obx->ObservationValue = obxData[5];
//		obx->Units = obxData[6];
//		obx->ReferencesRange = obxData[7];
//		obx->AbnormalFlags = obxData[8];
//		obx->ObservResultStatus = obxData[11];
//	}
//	else
//	{
//		obx = NULL;
//	}
//	return *obx;
//}