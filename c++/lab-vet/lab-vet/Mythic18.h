#include "KDevice.h"
#pragma once

#define MYTHIC		0
#define DATE		1
#define TIME		2
#define MODE		3
#define UNIT		4
#define SEQ			5
#define SID			6
#define PID			7
#define ID			8
#define TYPE		9
#define TEST		10
#define OPERATOR	11
#define WBC			12
#define RBC			13
#define HGB			14
#define HCT			15
#define PLT			16
#define LYM			17
#define MON			18
#define GRA			19
#define LYMPC		20
#define MONPC		21
#define GRAPC		22
#define MCV			23
#define MCH			24
#define MCHC		25
#define RDW			26
#define MPV			27
#define PCT			28
#define PDW			29
#define WBCconst	30

class Mythic18 : public KDevice
{
public:
	Mythic18(int port) : KDevice("Mythic18", port, "UDP") {}
	~Mythic18();

	int udpReceive();
};

