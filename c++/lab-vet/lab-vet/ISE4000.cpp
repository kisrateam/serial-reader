#include "ISE4000.h"

ISE4000::~ISE4000()
{
}

bool is_number(const std::string& s)
{
	return(strspn(s.c_str(), "-.0123456789") == s.size());
}

int ISE4000::readSerial()
{
	char c = 0;
	char packetBuff[1024] = "";
	char packetData[1024] = "";
	int packetLength = 0;
	int readResult = 0;
	vector<string> data;

	while (this->SP->IsConnected())
	{
		try
		{
			readResult = this->SP->ReadData(&c);
		}
		catch (const std::exception& e)
		{
			KUtils::write_text_to_log_file("read serial\n" + string(e.what()), this->name, true, true);
		}

		if (readResult > 0)
		{
			string s(1, c);
			KUtils::write_text_to_log_file(s, this->name, false, false);
			if (c)
			{
				packetBuff[packetLength] = c;
				packetLength++;
			}
			if (c == 0x0A && packetBuff[packetLength - 2] == 0x0D)
			{
				data = KUtils::splitWord(packetBuff);
				if (data.size() >= 9)
				{
					for (int i = 0; i < data.size(); i++)
					{
						if (data[i].length() == 18)
						{
							string barcode = KUtils::setBarcodeFormat(data[i], 10);
							jsonObj->addOrder(barcode, "", SP->name, "", "", "");
							jsonObj->addResult("K", data[i + 2], "mmol/L", "", "", "", "", "");
							jsonObj->addResult("Na", data[i + 3], "mmol/L", "", "", "", "", "");
							jsonObj->addResult("Cl", data[i + 4], "mmol/L", "", "", "", "", "");
							jsonObj->addResult("CO2", data[i + 7], "mmol/L", "", "", "", "", "");
							break;
						}
					}
					jsonObj->finish();
					api->jsonData = jsonObj->jsonData;
					cout << "Response = " << this->api->resultRawData() << endl;
				}
				jsonObj->clear();
				memset(packetBuff, 0, sizeof packetBuff);
				packetLength = 0;
			}
		}
	}
	return 0;
}


