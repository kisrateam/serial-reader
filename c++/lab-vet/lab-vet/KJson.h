#pragma once
#include <iostream>
#include <cstring>
#include "json.hpp"

using json = nlohmann::json;

using namespace std;

class KJson
{
public:
	KJson();
	~KJson();

	struct CS400Request
	{
		string number;
		string name;
		string sex;
		string age;
		string age_unit;
		string barcode;
		std::vector<string> orders;
	};

	string addOrder(string barcode, string dateTime, string deviceName, string mode, string type, string test);
	string addOrderBS620(string barcode, string dateTime, string deviceName, string mode, string type, string test);
	string addResult(string test, string value, string unit, string flag, string lowPanic, string low, string high, string highPanic);
	string addResultBS620(string test, string value, string unit, string flag, string lowPanic, string low, string high, string highPanic);
	void jsonDecodeCS400(string str);

	void finish();
	void clear();

	string jsonData;
	string defaultData = "{\"number\":\"00000000\",\"name\":\"ERROR BARCODE\",\"sex\":\"M\",\"age\":\"2\",\"age_unit\":\"Y\",\"barcode\":\"0000000000\",\"orders\":[\"ALT\"]}";
	CS400Request cs400Request;




};

