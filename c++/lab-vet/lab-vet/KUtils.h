#pragma once
#include <string>
#include <vector>
#include <sstream>
#include <iomanip>
#include <codecvt>
#include <locale>
#include <fstream>
#include <iostream>

using namespace std;

class KUtils
{
public:
	static vector<string> split(string str, char delimiter);
	static vector<string> splitWord(string str);
	static string setBarcodeFormat(string barcode, int position);
	static string utf8_to_string(const char * utf8str, const locale & loc);
	static void write_text_to_log_file(const std::string & text, string filename, bool date, bool endline);
};

