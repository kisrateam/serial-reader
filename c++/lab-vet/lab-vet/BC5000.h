#include "KDevice.h"
#pragma once
class _MSH2
{
public:
	string	FieldSeparator;
	string	EncodingCharacters;
	string	Sendingapplication;
	string	SendingFacility;
	string	DateTimeOfMessage;
	string	MessageType;
	string	MessageControlID;
	string	ProcessingID;
	string	VersionID;
	string	CharacterSet;
};

class _PID2
{
public:
	string	SetIDPID;
	string	PatientIdentifierList;
	string	PatientName;
	string	DateTimeofBirth;
	string	Sex;
};

class _PV12
{
public:
	string	SetIDPV1;
	string	PatientClass;
	string	AssignedPatientLocation;
	string	FinancialClass;
};

class _OBR2
{
public:
	string	SetIDOBR;
	string	PlacerOrderNumber;
	string	FillerOrderNumber;
	string	UniversalServiceID;
	string	RequestedDatetime;
	string	ObservationDateTime;
	string	CollectorIdentifier;
	string	RelevantClinicalInfo;
	string	SpecimenReceivedDateTime;
	string	SpecimenSource;
	string	ResultsRptStatusChngDateTime;
	string	DiagnosticServSectID;
	string	ResultCopiesTo;
	string	PrincipalResultInterpreter;
};

class _OBX2
{
public:
	string	SetIDOBX;
	string	ValueType;
	string	ObservationIdentifier;
	string	ObservationValue;
	string	Units;
	string	ReferencesRange;
	string	AbnormalFlags;
	string	ObservResultStatus;

};

class BC5000 : public KDevice
{

public:
	BC5000(int port) : KDevice("BC5000", port, "TCP") {}
	~BC5000();

	const char pipe = '|';
	const char caret = '^';
	const char back_slash = '\\';
	const char subcomponent_delimiter = '&';
	const char repetition_delimiter = '~';
	string delimiterDef = "|\\^&";
	int tcpReceive();
	//client
	void decodeProcess(string buffer);
	//server
	//void decodeProcess();
	char* getIP();
	_MSH2 decodeMSH2(string field);
	_PID2 decodePID2(string field);
	_PV12 decodePV12(string field);
	_OBR2 decodeOBR2(string field);
	_OBX2 decodeOBX2(string field);
};



