#include "KDevice.h"

class URIT500C : public KDevice
{
public:
	URIT500C(string comport, int buadrate) : KDevice("URIT500C", comport, buadrate)	{}
	~URIT500C();

	int readSerial();
	void makeValue(std::string &value);
	void makeUnit(std::string &unit);
};

