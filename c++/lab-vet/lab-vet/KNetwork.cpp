#include "KNetwork.h"


KNetwork::KNetwork(string name, int port, string protocal)
{
	int ret = WSAStartup(MAKEWORD(2, 2), &data);
	if (ret != 0)
		throw system_error(WSAGetLastError(), system_category(), "WSAStartup Failed");

	if (protocal == "TCP")
	{
		cout << "Server starting..." << endl;
		start_server(port);
	}
	else if (protocal == "UDP")
	{
		initUDP(port);
	}

	if (sock == INVALID_SOCKET)
		throw system_error(WSAGetLastError(), system_category(), "Error opening socket");
	else
	{
		cout << name << " connected\n" << endl;
	}
}

void KNetwork::initTCP(int port)
{
	sock = socket(AF_INET, SOCK_STREAM, 0);
	Bind(port);
	SetOption(1, TCP_KEEPALIVE);
	SetOption(1, TCP_KEEPIDLE);
	Listen();
}

void KNetwork::start_server(int port) {
	int wsaresult, i = 1;
	WSADATA wsaData;
	//const char* ipv4AddrStr = "11.0.0.1";
	//struct in_addr ipv4Addr;
	//server.sin_family = AF_INET;
	//server.sin_addr.s_addr = inet_pton(AF_INET, ipv4AddrStr, &ipv4Addr);
	server.sin_family = AF_INET;
	server.sin_addr.s_addr = INADDR_ANY;
	server.sin_port = htons(port);
	// Initialize Winsock
	wsaresult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	//if error
	if (wsaresult != 0) {
		printf("WSAStartup failed with error: %d\n", wsaresult);
	}
	// Create a SOCKET for connecting to server
	server_socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (server_socket == INVALID_SOCKET) {

		printf("socket failed with error: %ld\n", WSAGetLastError());
		WSACleanup();
	}
	setsockopt(server_socket, SOL_SOCKET, SO_REUSEADDR, (char *)&i, sizeof(i));
	//Binding part
	wsaresult = bind(server_socket, (sockaddr*)&server, sizeof(server));
	if (wsaresult == SOCKET_ERROR) {
		printf("bind failed with error: %d\n", WSAGetLastError());
		closesocket(server_socket);
		WSACleanup();
	}
	// Setup the TCP listening socket
	wsaresult = listen(server_socket, 5);
	unsigned long b = 1;
	//make it non blocking
	ioctlsocket(server_socket, FIONBIO, &b);
	if (wsaresult == SOCKET_ERROR) {
		printf("listen failed with error: %d\n", WSAGetLastError());
		closesocket(server_socket);
		WSACleanup();
	}
}

void KNetwork::initUDP(int port)
{
	sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	Bind(port);
}



KNetwork::~KNetwork()
{
	WSACleanup();
	closesocket(sock);
}

void KNetwork::SendTo(const string& address, unsigned short port, const char* buffer, int len, int flags)
{
	sockaddr_in add;
	add.sin_family = AF_INET;

	add.sin_port = htons(port);

	if (inet_pton(AF_INET, address.c_str(), &add.sin_addr.s_addr) < 0)
		printf("inet_pton error");

	int ret = sendto(sock, buffer, len, flags, reinterpret_cast<SOCKADDR *>(&add), sizeof(add));
	if (ret < 0)
		throw system_error(WSAGetLastError(), system_category(), "sendto failed");
}

void KNetwork::SendTo(sockaddr_in& address, const char* buffer, int len, int flags)
{
	int ret = sendto(sock, buffer, len, flags, reinterpret_cast<SOCKADDR *>(&address), sizeof(address));
	if (ret < 0)
		throw system_error(WSAGetLastError(), system_category(), "sendto failed");
}

sockaddr_in KNetwork::RecvFrom(char* buffer, int len, int flags)
{
	sockaddr_in from;
	int size = sizeof(from);
	int ret = recvfrom(sock, buffer, len, flags, reinterpret_cast<SOCKADDR *>(&from), &size);
	if (ret < 0)
		throw system_error(WSAGetLastError(), system_category(), "recvfrom failed");

	// make the buffer zero terminated
	buffer[ret] = 0;
	return from;
}

int KNetwork::Receive(SOCKET new_socket, char* buffer)
{
	int ret = recv(new_socket, buffer, 4096, 0);
	if (ret > 0) {
		printf("Bytes received: %d\n", ret);
		return ret;
	}
	else if (ret == 0)
	{
		printf("Connection closing...\n");
		return ret;
	}
	else {
		printf("recv failed with error: %d\n", WSAGetLastError());
		return 1;
	}
}

void KNetwork::SetOption(int opt, int flag) {
	if (setsockopt(sock, SOL_SOCKET, opt, (const char *)&flag, sizeof(flag))) { perror("ERROR: setsocketopt(), SO_KEEPIDLE"); exit(0); };
}

void KNetwork::Bind(unsigned short port)
{

	add.sin_family = AF_INET;
	add.sin_addr.s_addr = htonl(INADDR_ANY);
	add.sin_port = htons(port);

	int ret = bind(sock, reinterpret_cast<SOCKADDR *>(&add), sizeof(add));
	if (ret < 0)
	{
		throw system_error(WSAGetLastError(), system_category(), "Bind failed");
	}
}

void KNetwork::Listen() {
	if (listen(sock, 3) < 0)
	{
		perror("listen");
		exit(EXIT_FAILURE);
	}
}

SOCKET KNetwork::Accept() {
	int addrlen = sizeof(add);
	SOCKET new_socket;
	if ((new_socket = accept(sock, (struct sockaddr *)&add,
		(socklen_t*)&addrlen)) < 0)
	{
		perror("accept");
		exit(EXIT_FAILURE);
	}
	return new_socket;
}
