#include "C380.h"

C380::~C380()
{

}

void C380::ack()
{
	char ack = ACK;
	this->SP->WriteData(&ack, 1);
	KUtils::write_text_to_log_file("ACK", this->name, true, true);
}

string C380::CheckSum(string s)
{

	try {
		byte checksum = 0;
		char hex_str[3];
		for (int i = 0; i <= (int)s.length(); i++)
		{
			checksum += s[i];
		}
		sprintf(hex_str, "%02X", checksum);

		return hex_str;
	}
	catch (const std::exception& e) {
		KUtils::write_text_to_log_file("CheckSum\n" + string(e.what()), this->name, true, true);
	}
}

string C380::Packaging(string record)
{
	string strSum = "";
	record = record + (char)CR + (char)ETX;
	strSum = CheckSum(record);
	record = (char)STX + record + strSum + (char)CR + (char)LF;
	return record;
}

string C380::HeaderPacket(int recordNumber)
{
	string record = "";
	record = to_string(recordNumber) + "H" + pipe + delimiterDef;

	return Packaging(record);
}

string C380::PatientInformationPacket(int recordNumber, int sequenceNumber, string sample_id, string name, string sex, string age, string age_unit)
{
	string record = "";
	record = to_string(recordNumber) + "P" + pipe + to_string(sequenceNumber) + string(2, pipe) + sample_id + string(2, pipe) + name + string(3, pipe) + sex + string(6, pipe) + age + caret + age_unit;
	return Packaging(record);
}

string C380::TestOrderPacket(int recordNumber, int sequenceNumber, string sample_id, string sample_no, string disk_id, string position_no, string diluent, string universal_test, string piority, string specimen_descriptor, string report_type)
{
	time_t now = time(0);
	tm* ltm = localtime(&now);
	char date_time[15];
	sprintf(date_time, "%d%02d%02d%02d%02d%02d", ltm->tm_year + 1900, ltm->tm_mon + 1, ltm->tm_mday, ltm->tm_hour, ltm->tm_min, ltm->tm_sec);
	string record = "";
	record = to_string(recordNumber) + "O" + pipe + to_string(sequenceNumber) + pipe + sample_id + caret + sample_no + caret + disk_id + caret + position_no + caret + diluent + string(2, pipe) + string(3, caret) + universal_test + pipe + piority + pipe + date_time + string(9, pipe) + specimen_descriptor + string(10, pipe) + report_type;

	return Packaging(record);
}

string C380::TerminatorPacket(int recordNumber, int sequenceNumber, string termination_code)
{
	string record = "";
	record = to_string(recordNumber) + "L" + pipe + to_string(sequenceNumber) + pipe + termination_code;
	return Packaging(record);
}

void C380::write(KSerial* sp, string str)
{
	const char* packet = str.c_str();
	char c;
	while (!sp->ReadData(&c));
	if (c == ACK)
	{
		KUtils::write_text_to_log_file("Receive ACK", this->name, true, true);
		sp->WriteData(packet, (int)str.length());
	}
}

void C380::clearBuffer(char  packetBuff[1024], char  packetData[1024])
{
	memset(packetBuff, 0, sizeof packetBuff);
	memset(packetData, 0, sizeof packetData);
}

int C380::readSerial()
{

	char c = 0;
	char packetBuff[1024] = "";
	char all_text[1024] = "";
	char packetData[1024] = "";
	int packetLength = 0;
	int packetCheck = 0;
	byte sum = 0;
	int dataLength = 1023;
	int readResult = 0;
	int state = 0;
	char checksumStr[3] = "";
	char recordType = 0;
	char mode = 0;
	char requestOrderSum = 1;
	string diskNo = "";
	string posNo = "";
	string patient_name = "";
	string test1 = "";
	string test2 = "";
	string test3 = "";
	string test4 = "";
	bool isBusy = false;
	clock_t time_start = clock();


	vector<string> specimen;
	vector<string> instrumentSpecimentID;
	vector<string> universalTestID;
	vector<string> sample;
	vector<string> refRange;
	vector<string> MeasurementValue;
	vector<string> MeasurementRange;
	string MeasurementRangeUpperLimit;
	string MeasurementRangeLowerLimit;
	vector<string> normalRange;
	vector<string> panicRange;
	vector<vector<string>> requestOrder;
	vector<string> field;
	vector<string> all_list;

	int counter = 0;
	while (this->SP->IsConnected())
	{
		try
		{
			readResult = this->SP->ReadData(&c);
		}
		catch (const std::exception& e)
		{
			KUtils::write_text_to_log_file("read serial\n" + string(e.what()), this->name, true, true);
		}

		bool test = true;
		if (readResult > 0)
		{
			string s(1, c);
			KUtils::write_text_to_log_file(s, this->name + "_serial", false, false);
			if (c == ENQ)
			{
				isBusy = true;
				KUtils::write_text_to_log_file("Receive ACK", this->name, true, true);
				sum = 0;
				packetLength = 0;
				state = 0;
				ack();
			}
			else if (c == STX)
			{
				sum = 0;
				packetLength = 0;
				state = 1;
			}
			else if (c == EOT)
			{
				KUtils::write_text_to_log_file("Received EOT", this->name, true, true);
				if (mode == 'Q')
				{
					const char enq = ENQ;
					const char eot = EOT;
					int record_sum = 1;
					int sequence = 1;
					string str = "";
					KJson::CS400Request data;
					for (int i = 0; i < (int)requestOrder.size(); i++)
					{
						const string barcode = requestOrder[i][0];
						if (barcode.length() == 0)
						{
							jsonObj->jsonDecodeCS400(jsonObj->defaultData);
							data = jsonObj->cs400Request;
						}
						else
						{
							string jsonData;
							CURLcode responseCode = this->api->requestCS400(barcode, &jsonData);
							if (!responseCode)
							{
								if (jsonData == "404")
								{
									cout << "404" << endl;
									jsonObj->jsonDecodeCS400(jsonObj->defaultData);
									data = jsonObj->cs400Request;
								}
								else
								{
									jsonObj->jsonDecodeCS400(jsonData);
									data = jsonObj->cs400Request;
								}
							}
							else
							{
								jsonObj->jsonDecodeCS400(jsonObj->defaultData);
								data = jsonObj->cs400Request;
							}
						}

						//send ENQ
						SP->WriteData(&enq, 1);
						//send header
						str = HeaderPacket(record_sum % 8);
						record_sum++;
						write(SP, str);
						//send patient
						string ansiName = KUtils::utf8_to_string(data.name.c_str(), locale(".874"));
						str = PatientInformationPacket(record_sum % 8, sequence, data.number, ansiName, data.sex, "", "");
						record_sum++;
						write(SP, str);

						// send Test order
						for (int j = 0; j < (int)jsonObj->cs400Request.orders.size(); j++)
						{
							str = TestOrderPacket(record_sum % 8, sequence, barcode, "", requestOrder[i][2], requestOrder[i][3], "N", data.orders[j], "R", "1", "O");
							record_sum++;
							sequence++;
							write(SP, str);
						}
						// send Terminator
						sequence = 1;
						str = TerminatorPacket(record_sum % 8, sequence, "N");
						record_sum++;
						write(SP, str);
						//send EOT
						SP->WriteData(&eot, 1);

						//reset quantity of sequence and record 
						sequence = 1;
						record_sum = 1;
					}
					requestOrder.clear();
				}
				mode = 0;
				sum = 0;
				packetLength = 0;
				state = 0;
				clearBuffer(packetBuff, packetData);
				isBusy = false;
				time_start = clock();
			}

			if (state)
			{
				packetBuff[packetLength] = c;
				packetLength++;
				sum += c;

				if (packetBuff[packetLength - 3] == ETX)
				{
					sum = sum - STX - packetBuff[packetLength - 2] - packetBuff[packetLength - 1];
					memcpy(checksumStr, &packetBuff[packetLength - 2], 2);
					int a = std::stoi(checksumStr, 0, 16);
					if (sum != a)
					{
						state = 0;
						sum = 0;
						packetLength = 0;
						continue;
					}
					else
					{
						memcpy(packetData, &packetBuff[1], packetLength - 5);

						//CR
						all_list = KUtils::split(packetData, 0x0D);
						//LF
						//all_list = KUtils::split(packetData, 0x0A);

						int check_first = 0;
						for (auto& s : all_list) {
							if (check_first == 0) {
								recordType = s[1];
								std::cout << s[1] << std::endl;
							}
							else {
								recordType = s[0];
								std::cout << s[0] << std::endl;
							}
							check_first++;

							field = KUtils::split(s, '|');

							switch (recordType)
							{
							case 'H':
								KUtils::write_text_to_log_file("Received Header Record", this->name, true, true);
								//ack();
								break;
							case 'P':
								KUtils::write_text_to_log_file("Received Patient Record ", this->name, true, true);
								patient_name = field[5];
								//ack();
								break;
							case 'O':
								KUtils::write_text_to_log_file("Received Order Record", this->name, true, true);

								specimen = KUtils::split(field[2], '^');
								instrumentSpecimentID = KUtils::split(field[3], '^');


								if (patient_name == "QC")
								{
									jsonObj->addOrder(specimen[0], field[6], this->name, patient_name, instrumentSpecimentID[0], instrumentSpecimentID[1]);
								}
								else
								{
									jsonObj->addOrder(specimen[0], field[6], this->name, "", "", "");
								}
								//ack();
								break;
							case 'R':
								mode = recordType;
								KUtils::write_text_to_log_file("Received Result Record", this->name, true, true);

								//old
								universalTestID = KUtils::split(field[2], '^');
								refRange = KUtils::split(field[5], '\\');
								normalRange = KUtils::split(refRange[0], '^');
								panicRange = KUtils::split(refRange[1], '^');

								//new
								//MeasurementValue = KUtils::split(refRange[3], '^');
								//MeasurementRange = KUtils::split(field[5], '\\');
								//MeasurementRangeUpperLimit = MeasurementRange[0];
								//MeasurementRangeLowerLimit = MeasurementRange[1];

								//old
								if (patient_name == "QC")
								{
									//jsonObj->addResult(universalTestID[3], field[3], field[4], field[6], panicRange[0], normalRange[0], normalRange[1], field[7]);
									jsonObj->addResult("test", "0", "0", "0", "0", "0", "0", "0");
								}
								else
								{
									//jsonObj->addResult(universalTestID[3], field[3], field[4], field[6], panicRange[0], normalRange[0], normalRange[1], panicRange[1]);
									jsonObj->addResult("test", "0", "0", "0", "0", "0", "0", "0");
								}

								//ack();
								break;
							case 'L':
								KUtils::write_text_to_log_file("Received Terminate Record", this->name, true, true);
								if (mode == 'R')
								{
									jsonObj->finish();
									this->api->jsonData = jsonObj->jsonData;
									KUtils::write_text_to_log_file("\nResponse = " + to_string(this->api->resultRawData()), this->name, true, true);
									jsonObj->clear();
								}

								//ack();
								break;
							case 'Q':
								mode = recordType;
								KUtils::write_text_to_log_file("Received Request Information Record", this->name, true, true);

								sample = KUtils::split(field[2], '^');

								requestOrder.push_back(sample);
								//ack();
								break;
							default:
								break;
							}
						}

						state = 0;
						sum = 0;
						packetLength = 0;
					}
				}
			}
		}
		if (!isBusy)
		{
			clock_t elapse = (clock() - time_start) / 1000;
			if (elapse == 10)
			{
				ack();
				time_start = clock();
			}
		}
	}

	return 0;
}
